# Pocket Server (pocketserver)

This repo is a combination of scripts to run on your raspberrypi. It includes Docker and other Docker apps on your local network and links to tutorials on some of the setup and condenced written instructions.

## Installing OS, enabling SSH and configuring WiFi

* https://desertbot.io/blog/headless-raspberry-pi-4-ssh-wifi-setup
  * [Download Raspbian Buster Lite](https://www.raspberrypi.org/downloads/raspbian/)
  * [Download Etcher](https://www.balena.io/etcher/)
  * Plug your sd card into your computer, open Etcher and click "Select image"
    * Select the zip file of Buster Lite that you downloaded
    * Make sure the correct drive is selected and click Flash, you will be prompted for your computers password
  * When complete close Etcher, eject the sd and then reinsert the sd to your computer so we can edit some files
* Open terminal and run `cd ../../../Volumes/boot && touch ssh && touch wpa_supplicant.conf`
* Edit wpa_supplicant.conf to look like the [example.wpa_supplicant.conf](templates/wpa_supplicant.conf)
  * Modify the NETWORK-NAME and NETWORK-PASSWORD values to be your network connection values
* Exit terminal, eject the SD and put into the raspberrypi and boot
* Once the Pi is booted verify you can see it on your network

## Logging into the Pi via SSH & changing the default password

* Open terminal and run `ssh pi@raspberrypi.local`
  * Default password is: raspberrypi
* Change the default password
  * Run `passwd` while logged into the Pi

## Changeing the Pi hostname & setting a static IP

* Change the hostname
  * Run `sudo nano /etc/hostname` and change raspberrypi to your value (pocketserver for this example)
  * Run `sudo nano /etc/hosts` and change raspberrypi to your value (pocketserver for this example)
  * Reboot `sudo reboot`
  * You should now be able to ssh to your Pi by running `ssh pi@pocketserver.local`
* You can do additional changes by running `sudo raspi-config`
* Setting Static IP
  * https://electrondust.com/2017/11/25/setting-raspberry-pi-wifi-static-ip-raspbian-stretch-lite/
    * Edit `/etc/dhcpcd.conf`
      * Find the "Example static IP configuration:" section
        * Uncomment and change the values for interface, static ip_address, static routers, static domain_name_servers
        * Save the file and reboot the Pi using `sudo reboot`
        * Login to your Pi via SSH and run `ping google.com` to see if you have internet connectivity. If you get a response you are ready for the next steps, if not you will need to troubleshoot your network settings.

## Installing Git, Vim, Docker & Docker Compose

Were done with the inital setup, you should now have a Pi connected to your network and be able to access it by using SSH to either the static IP address or pi@pocketserver.local.

* Update & Upgrade the Pi
  * Run `sudo apt-get update -y && sudo apt-get upgrade -y`
* Install Git, Vim, Docker & Docker Compose
  * Run `sudo apt-get install git vim docker.io docker-compose -y`
  * Run `sudo usermod -aG docker $USER` so that you can use docker with your user without needing sudo
    * You will need to logout and back into your Pi for this setting to take effect
  * Login and verify the installed applications are there
    * Run `git --version` and verify the output is as expected
    * Run `vim --version` and verify the output is as expected
    * Run `docker --version` and verify the output is as expected
    * Run `docker-compose --version` and verify the output is as expected

## Using Docker

The [docker-compose.yml](docker-compose.yml) file is configured to run multiple containers. The application data for those containers is mapped to the volumes directory so you can restart, shutdown or upgrade your containers without loosing their data. Here are the pre-configured containers,

* [Portainer](https://www.portainer.io/)
  * Portainer is an open-source management toolset that allows you to easily build, manage and maintain Docker environments.
  * Configured IP: 192.168.1.100
  * Configured Ports: 9000
* [PiHole](https://pi-hole.net/)
  * The Pi-hole® is an open-source DNS sinkhole that protects your devices from unwanted content, without installing any client-side software.
  * Configured IP: 192.168.1.100
  * Configured Ports: 53, 67, 80, 443
  * Setup OpenDNS: https://www.youtube.com/watch?v=BSplICgr7iU
  * Setup PiHole: https://www.youtube.com/watch?v=dH3DdLy574M
* [Home Assistant](https://www.home-assistant.io/)
  * Home Assistant is an open-source home automation system that puts local control and privacy first.
  * Configured IP: 192.168.1.100
  * Configured Ports: 8123

## Version Control

I configure version control on the home directory within the raspberrypi. This setup is built for a specific purpose on my home network. Fortunately micro sd cards are cheap so if I want to run something else I just swap out the sd card with a different one.

You can skip this next step if you want to mange your version control differently.

CAUTION: we are about to make the home directory a git repo for pocketserver

* login to pi
* Run `git init`
* Run `vim .git/config` to edit the .git/config file to match this [example .git/config](templates/example.gitRepo.conf)
  * Update the <YOUR-REPO-GIT-URL> value
